package main

import ( 
	"fmt"
	//"strconv"
    "encoding/json"
)

const (
	CONF_FILE="/Users/josep/Documents/golang/routeswitch/conf.json"
)

type Class struct {
    Configs Config
    Servers Server
}

type Config map[string]string
type Server map[string]string


func main() {
    //data, err := ioutil.ReadFile(CONF_FILE)
    //check(err)
    //fmt.Printf("%s",data)
    resp, err := mkJson()
    check(err)
    //fmt.Fprintf(w, string(resp))
    fmt.Println(string(resp))
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func mkJson() ([]byte, error) {
    config := make(map[string]string)
    config["route"] = "1"
    server := make(map[string]string)
    server["4"] = "192.168.0.4"
    c := Class{config, server}
    return json.Marshal(c)
}