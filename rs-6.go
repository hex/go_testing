package main

import ( 
	"fmt"
	//"io"
	"io/ioutil"
	//"github.com/bitly/go-simplejson"
	"gopkg.in/yaml.v2"
)

const (
	CONF_FILE="/Users/josep/Documents/golang/routeswitch/conf.yaml"
)


func check(e error) {
    if e != nil {
        panic(e)
    }
}

type Conf struct {
	Key, Value string
}

func main() {
	data, err := ioutil.ReadFile(CONF_FILE)
	fmt.Printf("%s",data)
	//check(err)
	//fmt.Println(string(dat))
	conf := Conf{}
	err = yaml.Unmarshal(data, &conf)
	check(err)
	fmt.Printf("%#v\n",conf)
}
