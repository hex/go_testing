package main

import ( 
	"fmt"
	"os"
)

const (
	LOCK_FILE="/Users/josep/Documents/golang/routeswitch/lock"
)
	
func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {
	//Crear archiu
	f, err := os.Create(LOCK_FILE)
    check(err)
	defer f.Close()
	fmt.Println("file created")

	// buscar existencia archiu
	if _, err := os.Stat(LOCK_FILE); err == nil {
		fmt.Println("File exists")
	}

	// eliminar archiu
	err := os.Remove(LOCK_FILE);
	check(err)
}
