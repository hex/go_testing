package main

import(
	"fmt"
	"net/http"
	"os"
	"io/ioutil"
	"encoding/json"
	"time"
	"log"
)

const (
	CONF_FILE="conf.json"
	LOCK_FILE="rs.lock"
)

type Config struct {
    Mail string
    Routers Router
}

type Router []string


func main() {
	// Capturem el path
	path, err := os.Getwd()
	checkErr(err)
	// Captura el archiu de configuracio
    data, err := ioutil.ReadFile(path + "/" + CONF_FILE)
    checkErr(err)
    // Descodificant Json i creant els objectes pertinents
    var c Config
	err = json.Unmarshal(data, &c)
	checkErr(err)
	fmt.Println("Aixo es el mail: ", c.Mail)
	var routers Router
	routers = c.Routers


	//Comproba si esta bloquejat
    if(checkLocker(path)) {
    	//Esta bloquejat
    	fmt.Println("Bloquejat")
    } else {
    	fmt.Println("Programa no bloquejat, creant archiu")
    	//No esta bloquejat, bloqueja
    	makeLocker(path)

    	//i := 0;
    	//Comprova si esta conectat
    	test := false
    	//connectCheck()
    	for !test {
    		/*
    		i++
    		// Desconectat
			fmt.Println("Esta desconectat, fent feina")
			if i > 2 {
				break;
			}
			*/
			fmt.Println("Connection failed")
			for _, router := range(routers) {
				fmt.Print("Changing default route to: ", router);
				/*
				_, err := exec.Command("route","del","default","eth0").Output()
				checkErr(err)
				time.Sleep(2 * time.Second)
				_, err := exec.Command("route","add","default","gw",c[c[route]],"eth0").Output()
				checkErr(err)
				*/
				if router == "192.168.0.3" {
					test = true
				}
				fmt.Println(" OK!")
				time.Sleep(2 * time.Second)
				fmt.Print("Check connection... ")
				if test {
					fmt.Println(" OK!")
				} else {
					fmt.Println(" FAILED!")
				}
			}
		}

		//Desbloqueja
    	deleteLocker(path)
    	fmt.Println("Programa desbloquejat i finalitzat")
    }
}


// Chekeja conexio
func connectCheck() bool {
	if _, err := http.Get("http://google.com/"); err == nil  {
		return true
	} else {
		return false
	}
}

// Checkeja bloqueig
func checkLocker(path string) bool {
	if _, err := os.Stat(path + "/" + LOCK_FILE); err == nil {
		return true
	} else {
		return false
	}
}

// Bloqueja 
func makeLocker(path string) {
	f, err := os.Create(path + "/" + LOCK_FILE)
    checkErr(err)
	defer f.Close()
	fmt.Println("Archiu creat")
}

// Desbloqueja
func deleteLocker(path string) {
	err := os.Remove(path + "/" + LOCK_FILE);
	checkErr(err)
	fmt.Println("Archiu eliminat")
}

// Imprimeix error
func checkErr(e error) {
	if e != nil {
		log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
		log.Panic(e)
	}
}
