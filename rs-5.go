package main

import ( 
	"fmt"
	//"io"
	"io/ioutil"
	"github.com/bitly/go-simplejson"
)

const (
	CONF_FILE="/Users/josep/Documents/golang/routeswitch/conf.json"
)


func check(e error) {
    if e != nil {
        panic(e)
    }
}

type Config map[string]string

func main() {
	dat, err := ioutil.ReadFile(CONF_FILE)
	check(err)
	fmt.Println(string(dat))
	data, err := simplejson.NewJson(dat)
	check(err)
	var config Config = data;
		for index, ip := range config {
		fmt.Println("El elemento numero ",index," es",ip);
	}
}
