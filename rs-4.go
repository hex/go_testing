package main

import ( 
	"fmt"
	//"io"
	"io/ioutil"
	//"encoding/json"
)

const (
	CONF_FILE="/Users/josep/Documents/golang/routeswitch/conf.json"
)
	
func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {
	dat, err := ioutil.ReadFile(CONF_FILE)
	check(err)
	fmt.Print(string(dat))
}
