package main

import ( 
	"fmt"
	"strconv"
)

const (
	//CONF_FILE="/Users/josep/Documents/golang/routeswitch/conf.yaml"
)


func check(e error) {
    if e != nil {
        panic(e)
    }
}


func main() {
	config := map[string]string {
    	"route": "1",
    	"1":   "192.168.0.1",
		"2": "192.168.0.2",
		"3": "192.168.0.3",
	}
	fmt.Println(config["route"])
	fmt.Println(config["1"])

	for key, value := range config {
		fmt.Println(key, value)
	}

	for i := 1; i <= 3; i++ {
		fmt.Println("Servidors: ", config[strconv.Itoa(i)])
	}
}
