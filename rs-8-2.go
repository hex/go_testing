package main

import ( 
	"fmt"
	//"strconv"
    "encoding/json"
)

const (
	CONF_FILE="/Users/josep/Documents/golang/routeswitch/conf2.json"
)

type Config struct {
    Route int
    Mail string
    Routers Router
}

type Router map[string]string


func main() {
    //data, err := ioutil.ReadFile(CONF_FILE)
    //check(err)
    //fmt.Printf("%s",data)
    resp, err := mkJson()
    check(err)
    fmt.Println(string(resp))
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func mkJson() ([]byte, error) {
    route := 1
    mail := "ninpo85@gmail.com"
    routers := make(map[string]string)
    routers["1"] = "192.168.0.3"
    routers["3"] = "192.168.0.3"
    routers["4"] = "192.168.0.4"
    c := Config{route, mail, routers}
    //return json.Marshal(c)
    return json.MarshalIndent(c, "","  ")
}