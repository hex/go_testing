package main

import(
	"fmt"
	"net/http"
	"os"
	"io/ioutil"
	"encoding/json"
)

const (
	CONF_FILE="conf.json"
	LOCK_FILE="rs.lock"
)

type Config struct {
    Mail string
    Routers Router
}

type Router []string


func main() {
	// Capturem el path
	path, err := os.Getwd()
	checkErr(err)
	// Captura el archiu de configuracio
    data, err := ioutil.ReadFile(path + "/" + CONF_FILE)
    checkErr(err)
    // Descodificant Json i creant els objectes pertinents
    var c Config
	err = json.Unmarshal(data, &c)
	checkErr(err)
	fmt.Println("Aixo es el mail: ", c.Mail)
	var r Router
	r = c.Routers
	for key, value := range(r) {
		fmt.Println("Clau: ",key," Valor: ",value)
	}

	//Comproba si esta bloquejat
    if(checkLocker(path)) {
    	//Esta bloquejat
    	fmt.Println("Bloquejat")
    } else {
    	fmt.Println("Programa no bloquejat, creant archiu")
    	//No esta bloquejat, bloqueja
    	makeLocker(path)

    	//readConf()



		//Mostra genera un json
		//fmt.Print(makeJson())

    	/*
    	//Comprova si esta conectat
    	if (!connectCheck()) {
    		// Desconectat
			fmt.Println("Esta desconectat, fent feina")
		}
		*/


		//Desbloqueja
    	deleteLocker(path)
    	fmt.Println("Programa desbloquejat i finalitzat")
    }
}


// Chekeja conexio
func connectCheck() bool {
	if _, err := http.Get("http://google.com/"); err == nil  {
		return true
	} else {
		return false
	}
}

// Checkeja bloqueig
func checkLocker(path string) bool {
	if _, err := os.Stat(path + "/" + LOCK_FILE); err == nil {
		return true
	} else {
		return false
	}
}

// Bloqueja 
func makeLocker(path string) {
	f, err := os.Create(path + "/" + LOCK_FILE)
    checkErr(err)
	defer f.Close()
	fmt.Println("Archiu creat")
}

// Desbloqueja
func deleteLocker(path string) {
	err := os.Remove(path + "/" + LOCK_FILE);
	checkErr(err)
	fmt.Println("Archiu eliminat")
}


func changeRoute(c Config) {
			//cambia la ruta
			fmt.Println("route changed")
			/*
			_, err := exec.Command("route","del","default","eth0").Output()
			checkErr(err)
			_, err := exec.Command("route","add","default","gw",c[c[route]],"eth0").Output()
			checkErr(err)
			*/

			fmt.Println(c);
			//cambia el array
			//guarda archiu
}

func mailCheck() {
}

func makeJson() (string) {
    mail := "ninpo85@gmail.com"
    routers := Router{"192.168.0.1", "192.168.0.2", "192.168.0.3"}
    c := Config{mail, routers}
    //Si vols l'archiu sense identar
    //j, err = json.Marshal(c)
    j, err := json.MarshalIndent(c, "","  ")
    checkErr(err)
    return string(j)
}

func makeConf() {
}

// Imprimeix error
func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}
