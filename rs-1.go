package main

import ( 
	"fmt"
	"os/exec"
	//"log"
)

func main() {
	out, err := exec.Command("ls","-la").Output()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(string(out))
}
